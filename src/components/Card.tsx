import React from "react";
import homeimage from "../assets/homeImage.jpg";
import Image from "next/image";

interface CardContentProps {
  title: string;
  updatedAt: string;
  image: string;
}

const CardContent: React.FC<CardContentProps> = ({
  title,
  updatedAt,
  image,
}) => {
  const date = new Date(updatedAt);
  const options: any = { day: "numeric", month: "long", year: "numeric" };
  const formattedDate = date.toLocaleDateString("id-ID", options);
  return (
    <div className=" h-72 w-64 shadow-xl rounded-xl">
      <div>
        <img src={image} className="rounded-t-xl h-40 w-full" alt={image} />
      </div>
      <div className="p-5">
        <p className="text-gray-500">{formattedDate}</p>
        <p className="font-bold title">{title}</p>
      </div>
    </div>
  );
};

export default CardContent;
