"use client";

import Image from "next/image";
import React, { useEffect, useState } from "react";
import logo from "../assets/logo.png";

interface NavbarProps{
    defaultItem: string
}

const Navbar:React.FC<NavbarProps> = ({defaultItem}) => {
  const [scrolling, setScrolling] = useState(false);
  const [activeItem, setActiveItem] = useState<string>(defaultItem);

  useEffect(() => {
    const handleScroll = () => {
      const offset = window.scrollY;
      setScrolling(offset > 0);
    };

    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  const handleItemClick = (item: string) => {
    setActiveItem(item)
  }

  return (
    <div
      className={`flex justify-between items-center bg-oren h-20 px-40 fixed top-0 z-50 w-full transition-transform duration-300 ease-in-out transform ${
        scrolling ? "-translate-y-full" : "translate-y-0"
      }`}
    >
      <div>
        <Image src={logo} alt="logo" width={100} className="aspect-auto" />
      </div>
      <div className="flex text-white gap-x-5">
      <p
          className={`cursor-pointer ${
            activeItem === 'Work' ? 'border-b-2 border-white font-bold' : ''
          }`}
          onClick={() => handleItemClick('Work')}
        >
          Work
        </p>
        <p
          className={`cursor-pointer ${
            activeItem === 'About' ? 'border-b-2 border-white font-bold' : ''
          }`}
          onClick={() => handleItemClick('About')}
        >
          About
        </p>
        <p
          className={`cursor-pointer ${
            activeItem === 'Service' ? 'border-b-2 border-white font-bold' : ''
          }`}
          onClick={() => handleItemClick('Service')}
        >
          Service
        </p>
        <p
          className={`cursor-pointer ${
            activeItem === 'Ideas' ? 'border-b-2 border-white font-bold' : ''
          }`}
          onClick={() => handleItemClick('Ideas')}
        >
          Ideas
        </p>
        <p
          className={`cursor-pointer ${
            activeItem === 'Careers' ? 'border-b-2 border-white font-bold' : ''
          }`}
          onClick={() => handleItemClick('Careers')}
        >
          Careers
        </p>
        <p
          className={`cursor-pointer ${
            activeItem === 'Contact' ? 'border-b-2 border-white font-bold' : ''
          }`}
          onClick={() => handleItemClick('Contact')}
        >
          Contact
        </p>
      </div>
    </div>
  );
};

export default Navbar;
