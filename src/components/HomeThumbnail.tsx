"use client";

import { useEffect, useRef, useState } from "react";
import homeImage from "../assets/homeImage.jpg";

const HomeThumbnail = () => {
  const [scrollY, setScrollY] = useState<number>(0);
  const parallaxText = useRef<HTMLDivElement | null>(null);

  const handleScroll = () => {
    setScrollY(window.scrollY);
  };

  useEffect(() => {
    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  useEffect(() => {
    if (parallaxText.current) {
      const screenWidth = window.innerWidth;
      if (screenWidth > 780) {
        parallaxText.current.classList.remove("disabled-parallax");

        parallaxText.current.style.transform = `translateY(+${
          scrollY * 0.5
        }px)`;
      } else {
        parallaxText.current.classList.add("disabled-parallax");
      }
    }
  }, [scrollY]);

  return (
    <div
      className="h-screen w-full flex flex-col items-center justify-center bg-fixed"
      style={{
        backgroundImage: `url("https://suitmedia.static-assets.id/storage/files/601/6.jpg")`,
        backgroundSize: "cover",
        backgroundPosition: "center",
        position: "relative",
      }}
    >
      <div ref={parallaxText} className="flex flex-col items-center">
        <p className="text-white text-[100px]">Ideas</p>
        <p className="text-white text-2xl">Where all our great things begin</p>
      </div>
    </div>
  );
};

export default HomeThumbnail;
