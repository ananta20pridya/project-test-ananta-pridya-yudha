"use client";

import React, { useEffect, useState } from "react";
import { getAllResponse } from "../../api";
import DataPagination from "./Pagination";
import CardContent from "./Card";
import { Spin } from "antd";

type meta = {
  to: number;
  total: number;
  per_page:number
};

const CardList = () => {
  const [data, setData] = useState<any>();
  const [meta, setMeta] = useState<meta>();
  const [pageData, setPageData] = useState(1);
  const [pageSizeData, setPageSizeData] = useState(10);
  const [query, setQuery] = useState("-published_at");
  useEffect(() => {
    const fetchData = async () => {
      const response = await getAllResponse(pageData, pageSizeData, query);
      setData(response.data);
      setMeta(response.meta);
    };
    fetchData();
  }, [pageData, pageSizeData, query]);
  const handlePaginationChange = async (page: number, pageSize: number) => {
    setPageData(page);
    setPageSizeData(pageSize);
    console.log({ page, pageSize });
  };
  return (
    <div className="bg-white z-20 h-full">
      {data ? (
        <div className="2xl:mx-40 h-full bg-white flex flex-col justify-between">
          <div>
            <div className="flex justify-between mx-20 mb-5">
              <p>
                <span className="font-bold">Showing: </span>{meta? meta.to - meta.per_page + 1: ''}-
                {meta ? meta.to : ""} of {meta ? meta.total : ""}
              </p>
              <div className="flex gap-x-5 items-center">
                <p>Sort: </p>
                <select
                  value={query}
                  onChange={(e) => setQuery(e.target.value)}
                  className="border border-black py-1 px-3 rounded-xl"
                >
                  <option value={"-published_at"}>Newest</option>
                  <option value={"published_at"}>Oldest</option>
                </select>
              </div>
            </div>
            <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 xl:grid-cols-4 2xl:grid-cols-5 place-items-center gap-y-7">
              {data.map((item: any) => {
                let urlImage;
                if (item.small_image && item.small_image.length > 0) {
                  urlImage = item.small_image[0].url;
                } else {
                  urlImage = "image";
                }
                return (
                  <div key={item.id}>
                    <CardContent
                      title={item.title}
                      updatedAt={item.published_at}
                      image={urlImage}
                    />
                  </div>
                );
              })}
            </div>
          </div>
          <div className="flex justify-center my-5">
            <DataPagination
              onChangePagination={handlePaginationChange}
              total={meta ? meta.total : 274}
            />
          </div>
        </div>
      ) : (
        <div>
          <Spin size="large">
            <div className="content" />
          </Spin>
        </div>
      )}
    </div>
  );
};

export default CardList;
