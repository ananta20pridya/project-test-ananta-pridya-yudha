import Navbar from "@/components/Navbar";
import homeImage from "../assets/homeImage.jpg";
import Image from "next/image";
import CardList from "@/components/CardList";
import HomeThumbnail from "@/components/HomeThumbnail";

export default function Home() {
  return (
    <main className="h-full">
      <Navbar defaultItem="Ideas" />

      <div
        className=""
        style={{ backgroundImage: `url('../assets/homeImage.jpg')` }}
      >
        <HomeThumbnail />
      </div>
      <div className="pt-10 h-screen">
        <CardList />
      </div>
    </main>
  );
}
